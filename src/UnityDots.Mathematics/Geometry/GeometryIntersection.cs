using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static partial class Geometry
    {
        public static bool IntersectRectPoint(float2 rectMin, float2 rectMax, float2 point)
        {
            return all((point >= rectMin) & (point <= rectMax));
        }

        public static bool IntersectRectCircle(float2 rectMin, float2 rectMax, float2 circleOrigin, float circleRadius)
        {
            // Find the closest point to the circle within the rectangle
            float closestX = clamp(circleOrigin.x, rectMin.x, rectMax.x);
            float closestY = clamp(circleOrigin.y, rectMin.y, rectMax.y);

            // Calculate the distance between the circle's center and this closest point
            float distanceX = circleOrigin.x - closestX;
            float distanceY = circleOrigin.y - closestY;

            // If the distance is less than the circle's radius, an intersection occurs
            float distanceSquared = (distanceX * distanceX) + (distanceY * distanceY);
            return distanceSquared < circleRadius * circleRadius;
        }

        public static bool IntersectRectLineSegment(float2 rectMin, float2 rectMax, float2 lineStart, float2 lineEnd)
        {
            // Prelimiary checks to rule out impossible intersections.
            if (lineStart.x < rectMin.x && lineEnd.x < rectMin.y)
            {
                return false;
            }
            if (lineStart.x > rectMax.x && lineEnd.x > rectMax.x)
            {
                return false;
            }
            if (lineStart.y < rectMin.y && lineEnd.y < rectMin.y)
            {
                return false;
            }
            if (lineStart.y > rectMax.y && lineEnd.y > rectMax.y)
            {
                return false;
            }

            // Segment inside the rectangle.
            if (IntersectRectPoint(rectMin, rectMax, lineStart) || IntersectRectPoint(rectMin, rectMax, lineEnd))
            {
                return true;
            }

            // Check intersection of line segment to rectangle borders.
            var topRight = new float2(rectMax.x, rectMin.y);
            if (IntersectLineSegment2d(rectMin, topRight, lineStart, lineEnd))
            {
                return true;
            }
            var bottomLeft = new float2(rectMin.x, rectMax.x);
            if (IntersectLineSegment2d(rectMin, bottomLeft, lineStart, lineEnd))
            {
                return true;
            }
            if (IntersectLineSegment2d(bottomLeft, rectMax, lineStart, lineEnd))
            {
                return true;
            }
            if (IntersectLineSegment2d(rectMax, topRight, lineStart, lineEnd))
            {
                return true;
            }

            return false;
        }

        public static bool IntersectLineSegment2d(float2 lineStart1, float2 lineEnd1, float2 lineStart2, float2 lineEnd2)
        {
            var da = lineEnd1 - lineStart1;
            var db = lineEnd2 - lineStart2;
            var dot = (da.x * db.y) - (da.y * db.x);
            if (dot == 0)
            {
                return false;
            }

            var c = lineStart2 - lineStart1;
            var t = ((c.x * db.y) - (c.y * db.x)) / dot;
            if (t < 0 || t > 1)
            {
                return false;
            }

            var u = ((c.x * da.y) - (c.y * da.x)) / dot;
            if (u < 0 || u > 1)
            {
                return false;
            }

            return true;
        }

        // Returns true if the AABB with the given min and max corners
        // intersects with a sphere with the given origin and radius.
        public static bool IntersectAABBSphere(float3 aabbMin, float3 aabbMax, float3 circleOrigin, float circleRadius)
        {
            var distanceSqr = circleRadius * circleRadius;
            if (circleOrigin.x < aabbMin.x)
            {
                var val = circleOrigin.x - aabbMin.x;
                distanceSqr -= val * val;
            }
            else if (circleOrigin.x > aabbMax.x)
            {
                var val = circleOrigin.x - aabbMax.x;
                distanceSqr -= val * val;
            }
            if (circleOrigin.y < aabbMin.y)
            {
                var val = circleOrigin.y - aabbMin.y;
                distanceSqr -= val * val;
            }
            else if (circleOrigin.y > aabbMax.y)
            {
                var val = circleOrigin.y - aabbMax.y;
                distanceSqr -= val * val;
            }
            if (circleOrigin.z < aabbMin.z)
            {
                var val = circleOrigin.z - aabbMin.z;
                distanceSqr -= val * val;
            }
            else if (circleOrigin.z > aabbMax.z)
            {
                var val = circleOrigin.z - aabbMax.z;
                distanceSqr -= val * val;
            }

            return distanceSqr > 0;
        }
    }
}
