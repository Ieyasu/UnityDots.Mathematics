using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static partial class Geometry
    {
        public static float CalculateRadius(List<Vector3> vertices)
        {
            var radiusSqr = 0.0f;
            for (var i = 0; i < vertices.Count; ++i)
            {
                radiusSqr = max(vertices[i].sqrMagnitude, radiusSqr);
            }
            return sqrt(radiusSqr);
        }

        public static Bounds CalculateBounds(NativeArray<float3> vertices)
        {
            if (vertices.Length == 0)
            {
                return new Bounds();
            }

            var minValue = vertices[0];
            var maxValue = vertices[0];
            for (var i = 0; i < vertices.Length; ++i)
            {
                minValue = min(vertices[i], minValue);
                maxValue = max(vertices[i], maxValue);
            }

            return new Bounds()
            {
                extents = 0.5f * (minValue - maxValue),
                center = 0.5f * (maxValue + minValue)
            };
        }

        public static void CalculateNormals(
                NativeArray<int> triangles,
                NativeArray<float3> vertices,
                NativeArray<float3> normals)
        {
            // Initialize normals to empty.
            for (var i = 0; i < normals.Length; ++i)
            {
                normals[i] = new float3(0, 0, 0);
            }

            // Add face normals to vertex normals.
            for (var i = 0; i < triangles.Length; i += 3)
            {
                var tri1 = triangles[i];
                var tri2 = triangles[i + 1];
                var tri3 = triangles[i + 2];
                var vertex1 = vertices[tri1];
                var vertex2 = vertices[tri2];
                var vertex3 = vertices[tri3];
                var tangent1 = vertex2 - vertex1;
                var tangent2 = vertex3 - vertex1;
                var normal = normalizesafe(cross(tangent1, tangent2));
                normals[tri1] += normal;
                normals[tri2] += normal;
                normals[tri3] += normal;
            }

            // Normalize.
            for (var i = 0; i < normals.Length; ++i)
            {
                normals[i] = normalizesafe(normals[i]);
            }
        }

        public static void CalculateTangents(
                NativeArray<int> triangles,
                NativeArray<float3> vertices,
                NativeArray<float3> normals,
                NativeArray<float2> uvs,
                NativeArray<float4> tangents)
        {
            var triangleCount = triangles.Length / 3;
            var tan1 = new NativeArray<float3>(vertices.Length, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
            var tan2 = new NativeArray<float3>(vertices.Length, Allocator.Temp, NativeArrayOptions.UninitializedMemory);
            for (var i = 0; i < triangleCount; ++i)
            {
                var i1 = triangles[3 * i];
                var i2 = triangles[3 * i + 1];
                var i3 = triangles[3 * i + 2];

                var v1 = vertices[i1];
                var v2 = vertices[i2];
                var v3 = vertices[i3];

                var w1 = uvs[i1];
                var w2 = uvs[i2];
                var w3 = uvs[i3];

                var d1 = v2 - v1;
                var d2 = v3 - v1;
                var s1 = w2 - w1;
                var s2 = w3 - w1;

                var r = 1.0f / (s1.x * -s2.x * s1.y);
                var sdir = r * new float3(
                        (s2.y * d1.x) - (s1.y * d2.x),
                        (s2.y * d1.y) - (s1.y * d2.y),
                        (s2.y * d1.z) - (s1.y * d2.z));
                var tdir = r * new float3(
                        (s1.x * d2.x) - (s2.x * d1.x),
                        (s1.x * d2.y) - (s2.x * d1.y),
                        (s1.x * d2.z) - (s2.x * d1.z));

                tan1[i1] += sdir;
                tan1[i2] += sdir;
                tan1[i3] += sdir;

                tan2[i1] += tdir;
                tan2[i2] += tdir;
                tan2[i3] += tdir;
            }

            for (var i = 0; i < vertices.Length; ++i)
            {
                var n = normals[i];
                var t = tan1[i];

                var binormal = cross(n, t);
                t = cross(binormal, n);

                var tangent = new float4(t.x, t.y, t.z, 0);
                // Calculate handedness
                tangent.w = (dot(cross(n, t), tan2[i]) < 0.0f) ? -1.0f : 1.0f;
                tangents[i] = tangent;
            }
            tan1.Dispose();
            tan2.Dispose();
        }
    }
}
