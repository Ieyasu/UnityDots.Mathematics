using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static partial class Geometry
    {
        // Lengths for different curves are calculated using the Gaussian quadrature numeric integration method.
        // https://en.wikipedia.org/wiki/Gaussian_quadrature
        private struct GaussLengendreCoefficient
        {
            public float Abscissa;
            public float Weight;
        };

        // https://pomax.github.io/bezierinfo/legendre-gauss.html
        private static readonly GaussLengendreCoefficient[] _coefficients4 =
        {
            new GaussLengendreCoefficient { Abscissa = 0.0f, Weight = 0.5688889f },
            new GaussLengendreCoefficient { Abscissa = -0.3399810435848563f,  Weight = 0.6521451548625461f },
            new GaussLengendreCoefficient { Abscissa = 0.3399810435848563f,  Weight = 0.6521451548625461f },
            new GaussLengendreCoefficient { Abscissa = -0.8611363115940526f,  Weight = 0.3478548451374538f },
            new GaussLengendreCoefficient { Abscissa = 0.8611363115940526f,  Weight = 0.3478548451374538f }
        };

        // https://pomax.github.io/bezierinfo/legendre-gauss.html
        private static readonly GaussLengendreCoefficient[] _coefficients5 =
        {
            new GaussLengendreCoefficient { Abscissa = 0.0f, Weight = 0.5688888888888889f },
            new GaussLengendreCoefficient { Abscissa = -0.5384693101056831f,  Weight = 0.4786286704993665f },
            new GaussLengendreCoefficient { Abscissa = 0.5384693101056831f,  Weight = 0.4786286704993665f },
            new GaussLengendreCoefficient { Abscissa = -0.9061798459386640f,  Weight = 0.2369268850561891f },
            new GaussLengendreCoefficient { Abscissa = 0.9061798459386640f,  Weight = 0.2369268850561891f }
        };

        public static float GetLengthBezierQuadratic(float p0, float p1, float p2)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients4.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients4[i].Abscissa);
                var derivative = InterpolateTangentBezierQuadratic(p0, p1, p2, t);
                len += length(derivative) * (0.5f * _coefficients4[i].Weight);
            }
            return len;
        }

        public static float GetLengthBezierQuadratic(float2 p0, float2 p1, float2 p2)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients4.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients4[i].Abscissa);
                var derivative = InterpolateTangentBezierQuadratic(p0, p1, p2, t);
                len += length(derivative) * (0.5f * _coefficients4[i].Weight);
            }
            return len;
        }

        public static float GetLengthBezierQuadratic(float3 p0, float3 p1, float3 p2)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients4.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients4[i].Abscissa);
                var derivative = InterpolateTangentBezierQuadratic(p0, p1, p2, t);
                len += length(derivative) * (0.5f * _coefficients4[i].Weight);
            }
            return len;
        }

        public static float GetLengthBezierCubic(float p0, float p1, float p2, float p3)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients5.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients5[i].Abscissa);
                var derivative = InterpolateTangentBezierCubic(p0, p1, p2, p3, t);
                len += length(derivative) * (0.5f * _coefficients5[i].Weight);
            }
            return len;
        }

        public static float GetLengthBezierCubic(float2 p0, float2 p1, float2 p2, float2 p3)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients5.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients5[i].Abscissa);
                var derivative = InterpolateTangentBezierCubic(p0, p1, p2, p3, t);
                len += length(derivative) * (0.5f * _coefficients5[i].Weight);
            }
            return len;
        }

        public static float GetLengthBezierCubic(float3 p0, float3 p1, float3 p2, float3 p3)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients5.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients5[i].Abscissa);
                var derivative = InterpolateTangentBezierCubic(p0, p1, p2, p3, t);
                len += length(derivative) * (0.5f * _coefficients5[i].Weight);
            }
            return len;
        }

        public static float GetLengthCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float alpha = 0.5f)
        {
            var len = 0.0f;
            for (var i = 0; i < _coefficients5.Length; ++i)
            {
                // 0.5f is required to change interval [0, 1] to [-1, 1].
                var t = 0.5f + (0.5f * _coefficients5[i].Abscissa);
                var derivative = InterpolateTangentCatmullRom(p0, p1, p2, p3, t, alpha);
                len += length(derivative) * (0.5f * _coefficients5[i].Weight);
            }
            return len;
        }

        // https://pomax.github.io/bezierinfo/#extremities
        public static float2 GetBoundsBezierCubic(float p0, float p1, float p2, float p3)
        {
            var boundsMin = min(p0, p3);
            var boundsMax = max(p0, p3);

            var a = 3 * (-p0 + 3 * p1 - 3 * p2 + p3);
            var b = 6 * (p0 - 2 * p1 + p2);
            var c = 3 * (p1 - p0);

            var s = sqrt(b * b - 4 * a * c) / (2 * a);
            var t1 = (-b + s) / (2 * a);
            var t2 = (-b - s) / (2 * a);

            if (t1 >= 0 && t1 <= 1)
            {
                var val = InterpolateBezierCubic(p0, p1, p2, p3, t1);
                boundsMin = min(boundsMin, val);
                boundsMax = max(boundsMax, val);
            }

            if (t2 >= 0 && t2 <= 1)
            {
                var val = InterpolateBezierCubic(p0, p1, p2, p3, t2);
                boundsMin = min(boundsMin, val);
                boundsMax = max(boundsMax, val);
            }

            return new float2(boundsMin, boundsMax);
        }

        // https://pomax.github.io/bezierinfo/#extremities
        public static float4 GetBoundsBezierCubic(float2 p0, float2 p1, float2 p2, float2 p3)
        {
            var boundsMin = min(p0, p3);
            var boundsMax = max(p0, p3);

            var a = 3 * (-p0 + 3 * p1 - 3 * p2 + p3);
            var b = 6 * (p0 - 2 * p1 + p2);
            var c = 3 * (p1 - p0);

            var s = sqrt(b * b - 4 * a * c) / (2 * a);
            var t1 = (-b + s) / (2 * a);
            var t2 = (-b - s) / (2 * a);

            if (t1.x >= 0 && t1.x <= 1)
            {
                var val = InterpolateBezierCubic(p0, p1, p2, p3, t1.x);
                boundsMin = min(boundsMin, val);
                boundsMax = max(boundsMax, val);
            }

            if (t1.y >= 0 && t1.y <= 1)
            {
                var val = InterpolateBezierCubic(p0, p1, p2, p3, t1.y);
                boundsMin = min(boundsMin, val);
                boundsMax = max(boundsMax, val);
            }

            if (t2.x >= 0 && t2.x <= 1)
            {
                var val = InterpolateBezierCubic(p0, p1, p2, p3, t2.x);
                boundsMin = min(boundsMin, val);
                boundsMax = max(boundsMax, val);
            }

            if (t2.y >= 0 && t2.y <= 1)
            {
                var val = InterpolateBezierCubic(p0, p1, p2, p3, t2.y);
                boundsMin = min(boundsMin, val);
                boundsMax = max(boundsMax, val);
            }

            return new float4(boundsMin, boundsMax);
        }

        public static float InterpolateBezierQuadratic(float p0, float p1, float p2, float t)
        {
            var tinv = 1 - t;
            return (p0 * (tinv * tinv)) +
                   (p1 * (2 * tinv * t)) +
                   (p2 * (t * t));
        }

        public static float2 InterpolateBezierQuadratic(float2 p0, float2 p1, float2 p2, float t)
        {
            var tinv = 1 - t;
            return (p0 * (tinv * tinv)) +
                   (p1 * (2 * tinv * t)) +
                   (p2 * (t * t));
        }

        public static float3 InterpolateBezierQuadratic(float3 p0, float3 p1, float3 p2, float t)
        {
            var tinv = 1 - t;
            return (p0 * (tinv * tinv)) +
                   (p1 * (2 * tinv * t)) +
                   (p2 * (t * t));
        }

        public static float InterpolateTangentBezierQuadratic(float p0, float p1, float p2, float t)
        {
            return ((p1 - p0) * (2 * (t - 1))) +
                   ((p2 - p1) * (2 * t));
        }

        public static float2 InterpolateTangentBezierQuadratic(float2 p0, float2 p1, float2 p2, float t)
        {
            return ((p1 - p0) * (2 * (t - 1))) +
                   ((p2 - p1) * (2 * t));
        }

        public static float3 InterpolateTangentBezierQuadratic(float3 p0, float3 p1, float3 p2, float t)
        {
            return ((p1 - p0) * (2 * (t - 1))) +
                   ((p2 - p1) * (2 * t));
        }

        public static float InterpolateBezierCubic(float p0, float p1, float p2, float p3, float t)
        {
            var tt = t * t;
            var tinv = 1 - t;
            var ttinv = tinv * tinv;
            return (p0 * (ttinv * tinv)) +
                   (p1 * (3.0f * ttinv * t)) +
                   (p2 * (3.0f * tinv * tt)) +
                   (p3 * (tt * t));
        }

        public static float2 InterpolateBezierCubic(float2 p0, float2 p1, float2 p2, float2 p3, float t)
        {
            var tt = t * t;
            var tinv = 1 - t;
            var ttinv = tinv * tinv;
            return (p0 * (ttinv * tinv)) +
                   (p1 * (3.0f * ttinv * t)) +
                   (p2 * (3.0f * tinv * tt)) +
                   (p3 * (tt * t));
        }

        public static float3 InterpolateBezierCubic(float3 p0, float3 p1, float3 p2, float3 p3, float t)
        {
            var tt = t * t;
            var tinv = 1 - t;
            var ttinv = tinv * tinv;
            return (p0 * (ttinv * tinv)) +
                   (p1 * (3.0f * ttinv * t)) +
                   (p2 * (3.0f * tinv * tt)) +
                   (p3 * (tt * t));
        }

        public static float InterpolateTangentBezierCubic(float p0, float p1, float p2, float p3, float t)
        {
            var tinv = 1 - t;
            return ((p1 - p0) * (3 * tinv * tinv)) +
                   ((p2 - p1) * (6 * tinv * t)) +
                   ((p3 - p2) * (3 * t * t));
        }

        public static float2 InterpolateTangentBezierCubic(float2 p0, float2 p1, float2 p2, float2 p3, float t)
        {
            var tinv = 1 - t;
            return ((p1 - p0) * (3 * tinv * tinv)) +
                   ((p2 - p1) * (6 * tinv * t)) +
                   ((p3 - p2) * (3 * t * t));
        }

        public static float3 InterpolateTangentBezierCubic(float3 p0, float3 p1, float3 p2, float3 p3, float t)
        {
            var tinv = 1 - t;
            return ((p1 - p0) * (3 * tinv * tinv)) +
                   ((p2 - p1) * (6 * tinv * t)) +
                   ((p3 - p2) * (3 * t * t));
        }

        public static float3 InterpolateCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float t)
        {
            // If no alpha is specified, 0.5 is used by default.
            var t0 = 0.0f;
            var t1 = sqrt(length(p1 - p0)) + t0;
            var t2 = sqrt(length(p2 - p1)) + t1;
            var t3 = sqrt(length(p3 - p2)) + t2;
            return InterpolateCatmullRom(p0, p1, p2, p3, t0, t1, t2, t3, t);
        }

        public static float3 InterpolateCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float t, float alpha)
        {
            if (alpha == 0.5f)
            {
                return InterpolateCatmullRom(p0, p1, p2, p3, t);
            }

            var t0 = 0.0f;
            var t1 = pow(length(p1 - p0), alpha) + t0;
            var t2 = pow(length(p2 - p1), alpha) + t1;
            var t3 = pow(length(p3 - p2), alpha) + t2;
            return InterpolateCatmullRom(p0, p1, p2, p3, t0, t1, t2, t3, t);
        }

        public static float3 InterpolateCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float t0, float t1, float t2, float t3, float t)
        {
            var td = ((1 - t) * t1) + (t * t2);
            var tt0 = td - t0;
            var tt1 = td - t1;
            var tt2 = td - t2;
            var t1t = t1 - td;
            var t2t = t2 - td;
            var t3t = t3 - td;
            var t1t0 = 1.0f / (t1 - t0);
            var t2t0 = 1.0f / (t2 - t0);
            var t2t1 = 1.0f / (t2 - t1);
            var t3t1 = 1.0f / (t3 - t1);
            var t3t2 = 1.0f / (t3 - t2);

            var pa0 = (t1t * t1t0 * p0) + (tt0 * t1t0 * p1);
            var pa1 = (t2t * t2t1 * p1) + (tt1 * t2t1 * p2);
            var pa2 = (t3t * t3t2 * p2) + (tt2 * t3t2 * p3);
            var pb0 = (t2t * t2t0 * pa0) + (tt0 * t2t0 * pa1);
            var pb1 = (t3t * t3t1 * pa1) + (tt1 * t3t1 * pa2);

            return (t2t * t2t1 * pb0) + (tt1 * t2t1 * pb1);
        }

        public static float3 InterpolateTangentCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float t)
        {
            // If no alpha is specified, 0.5 is used by default.
            var t0 = 0;
            var t1 = sqrt(length(p0 - p1)) + t0;
            var t2 = sqrt(length(p1 - p2)) + t1;
            var t3 = sqrt(length(p2 - p3)) + t2;
            return InterpolateTangentCatmullRom(p0, p1, p2, p3, t0, t1, t2, t3, t);
        }

        public static float3 InterpolateTangentCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float t, float alpha)
        {
            var t0 = 0;
            var t1 = pow(length(p0 - p1), alpha) + t0;
            var t2 = pow(length(p1 - p2), alpha) + t1;
            var t3 = pow(length(p2 - p3), alpha) + t2;
            return InterpolateTangentCatmullRom(p0, p1, p2, p3, t0, t1, t2, t3, t);
        }

        public static float3 InterpolateTangentCatmullRom(float3 p0, float3 p1, float3 p2, float3 p3, float t0, float t1, float t2, float t3, float t)
        {
            // http://denkovacs.com/2016/02/catmull-rom-spline-derivatives/
            var td = ((1 - t) * t1) + (t * t2);
            var tt0 = td - t0;
            var tt1 = td - t1;
            var tt2 = td - t2;
            var t1t = t1 - td;
            var t2t = t2 - td;
            var t3t = t3 - td;
            var t1t0 = 1.0f / (t1 - t0);
            var t2t0 = 1.0f / (t2 - t0);
            var t2t1 = 1.0f / (t2 - t1);
            var t3t1 = 1.0f / (t3 - t1);
            var t3t2 = 1.0f / (t3 - t2);

            var pa0 = (t1t * t1t0 * p0) + (tt0 * t1t0 * p1);
            var pa1 = (t2t * t2t1 * p1) + (tt1 * t2t1 * p2);
            var pa2 = (t3t * t3t2 * p2) + (tt2 * t3t2 * p3);
            var pb0 = (t2t * t2t0 * pa0) + (tt0 * t2t0 * pa1);
            var pb1 = (t3t * t3t1 * pa1) + (tt1 * t3t1 * pa2);

            var da0 = t1t0 * (p1 - p0);
            var da1 = t2t1 * (p2 - p1);
            var da2 = t3t2 * (p3 - p2);
            var db0 = (t2t0 * (pa1 - pa0)) + (t2t * t2t0 * da0) + (tt0 * t2t0 * da1);
            var db1 = (t3t1 * (pa2 - pa1)) + (t3t * t3t1 * da1) + (tt1 * t3t1 * da2);
            return (t2t1 * (pb1 - pb0)) + (t2t * t2t1 * db0) + (tt1 * t2t1 * db1);
        }
    }
}
