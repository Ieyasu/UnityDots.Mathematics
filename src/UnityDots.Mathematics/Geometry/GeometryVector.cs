using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static partial class Geometry
    {
        public static float3 TransformPoint(float3 pos, float3 translation, quaternion rotation, float3 scale)
        {
            return mul(rotation, pos * scale) + translation;
        }

        public static float3 TransformDirection(float3 dir, quaternion rotation, float3 scale)
        {
            var epsilon = 0.001f;
            if (scale.x >= 0 && scale.x < epsilon)
            {
                scale.x = epsilon;
            }
            else if (scale.x < 0 && scale.x > -epsilon)
            {
                scale.x = -epsilon;
            }
            if (scale.y >= 0 && scale.y < epsilon)
            {
                scale.y = epsilon;
            }
            else if (scale.y < 0 && scale.y > -epsilon)
            {
                scale.y = -epsilon;
            }
            if (scale.z >= 0 && scale.z < epsilon)
            {
                scale.z = epsilon;
            }
            else if (scale.z < 0 && scale.z > -epsilon)
            {
                scale.z = -epsilon;
            }
            return mul(rotation, dir / scale);
        }
    }
}
