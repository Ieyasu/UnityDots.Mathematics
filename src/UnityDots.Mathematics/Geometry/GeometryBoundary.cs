using System.Collections.Generic;
using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static partial class Geometry
    {
        public static float3 GetClosestPointOnPath(float3 point, IList<float3> path)
        {
            var closestPoint = point;
            var distance = float.MaxValue;
            for (var i = 0; i < path.Count - 1; ++i)
            {
                var candidatePoint = GetClosestPointOnLineSegment(path[i], path[i + 1], point);
                var candidateDistance = lengthsq(candidatePoint - point);
                if (candidateDistance < distance)
                {
                    distance = candidateDistance;
                    closestPoint = candidatePoint;
                }
            }
            return closestPoint;
        }

        public static float3 GetClosestPointOnLineSegment(float3 point, float3 lineStart, float3 lineEnd)
        {
            var delta = lineEnd - lineStart;
            var length = lengthsq(delta);
            if (length == 0.0)
            {
                return lineStart;
            }

            var t = dot(point - lineStart, delta) / length;
            if (t < 0.0)
            {
                return lineStart;
            }
            if (t > 1.0)
            {
                return lineEnd;
            }
            return lineStart + (t * delta);
        }

        public static float2 GetClosestPointOnLineSegment2d(float2 point, float2 lineStart, float2 lineEnd)
        {
            var delta = lineEnd - lineStart;
            var length = lengthsq(delta);
            if (length == 0.0f)
            {
                return lineStart;
            }

            var t = dot(point - lineStart, delta) / length;
            if (t < 0.0)
            {
                return lineStart;
            }
            if (t > 1.0)
            {
                return lineEnd;
            }
            return lineStart + (t * delta);
        }

        public static float GetDistanceToRect(float2 point, float2 rectMin, float2 rectMax)
        {
            return sqrt(GetDistanceSqrToRect(point, rectMin, rectMax));
        }

        public static float GetDistanceSqrToRect(float2 point, float2 rectMin, float2 rectMax)
        {
            var center = 0.5f * (rectMax + rectMin);
            var halfSize = 0.5f * (rectMax - rectMin);
            var delta = max(abs(center - point) - halfSize, 0);
            return lengthsq(delta);
        }

        public static float GetDistanceToCircleEdge(float2 point, float2 origin, float radius)
        {
            var distance = length(point - origin);
            return abs(distance - radius);
        }

        public static float GetDistanceToCircle(float2 point, float2 origin, float radius)
        {
            var distance = length(point - origin);
            return max(0, distance - radius);
        }

        public static float GetDistanceToLineSegment(float3 point, float3 start, float3 end)
        {
            var closestPoint = GetClosestPointOnLineSegment(point, start, end);
            return length(point - closestPoint);
        }

        public static float GetDistanceToLineSegment2d(float2 point, float2 start, float2 end)
        {
            var closestPoint = GetClosestPointOnLineSegment2d(point, start, end);
            return length(point - closestPoint);
        }

        public static float GetDistanceSqrToLineSegment(float3 point, float3 start, float3 end)
        {
            var closestPoint = GetClosestPointOnLineSegment(point, start, end);
            return lengthsq(point - closestPoint);
        }

        public static float GetDistanceSqrToLineSegment2d(float2 point, float2 start, float2 end)
        {
            var closestPoint = GetClosestPointOnLineSegment2d(point, start, end);
            return lengthsq(point - closestPoint);
        }
    }
}
