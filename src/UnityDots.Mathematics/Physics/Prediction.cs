using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static class Prediction
    {
        public static float3 PredictIntersectPosition(float3 targetPos, float3 targetVel, float3 sourcePos, float sourceSpeed)
        {
            var success = PredictIntersectDirection(targetPos, targetVel, sourcePos, sourceSpeed, out float3 direction, out float t);
            return success ? targetPos + (targetVel * t) : targetPos;
        }

        public static float3 PredictIntersectDirection(float3 targetPos, float3 targetVel, float3 sourcePos, float sourceSpeed)
        {
            var success = PredictIntersectDirection(targetPos, targetVel, sourcePos, sourceSpeed, out float3 direction, out float t);
            return success ? direction : normalizesafe(targetPos - sourcePos);
        }

        // Returns a direction for an object starting from the given position with
        // the given speed that intersects with a target with the given position
        // and velocity.
        public static bool PredictIntersectDirection(
                float3 targetPos,
                float3 targetVel,
                float3 sourcePos,
                float sourceSpeed,
                out float3 direction,
                out float t)
        {
            // Get coefficients for the quadratic equation that solves the problem.
            // Can be derived from two equations:
            // 1. targetPos + (targetVel * t) = sourcePos + (sourceDir * sourceSpeed * t)
            // 2. (targetPos - targetVel).magnitude = (sourceSpeed * t)^2
            var a = dot(targetVel, targetVel) - (sourceSpeed * sourceSpeed);
            var b = 2.0f * (dot(targetPos, targetVel) - dot(sourcePos, targetVel));
            var c = dot(targetPos, targetPos) + dot(sourcePos, sourcePos) - 2.0f * dot(sourcePos, targetPos);

            // If a=0, the equation is linear and can't be solved with
            // quadratic equation solver.
            if (abs(a) < 0.000001f)
            {
                t = -c / b;
                var tValid = t > 0 && !isinf(t);
                if (!tValid)
                {
                    direction = 0;
                    t = 0;
                    return false;
                }
            }
            else
            {
                // Solve the quadratic equation.
                var t1 = (-b + sqrt((b * b) - (4.0f * a * c))) / (2.0f * a);
                var t2 = (-b - sqrt((b * b) - (4.0f * a * c))) / (2.0f * a);

                // Check if the solutions for the quadratic equations are valid.
                var t1Valid = t1 > 0 && !isnan(t1) && !isinf(t1);
                var t2Valid = t2 > 0 && !isnan(t2) && !isinf(t2);
                if (t1Valid)
                {
                    if (t2Valid)
                    {
                        t = min(t1, t2);
                    }
                    else
                    {
                        t = t1;
                    }
                }
                else
                {
                    if (t2Valid)
                    {
                        t = t2;
                    }
                    else
                    {
                        direction = 0;
                        t = 0;
                        return false;
                    }
                }
            }

            direction = (targetPos - sourcePos + (t * targetVel)) / (t * sourceSpeed);
            return true;
        }
    }
}
