using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static class Math
    {
        public static int CeilToPowerOfTwo(float x)
        {
            return CeilToPowerOfTwo(ceil(x));
        }

        public static int CeilToPowerOfTwo(int x)
        {
            var next = CeilToNextPowerOfTwo(x);
            var prev = next >> 1;
            return next - x < x - prev ? next : prev;
        }

        public static int CeilToNextPowerOfTwo(int x)
        {
            if (x < 0)
            {
                return 0;
            }
            --x;
            x |= x >> 1;
            x |= x >> 2;
            x |= x >> 4;
            x |= x >> 8;
            x |= x >> 16;
            return x + 1;
        }
    }
}
