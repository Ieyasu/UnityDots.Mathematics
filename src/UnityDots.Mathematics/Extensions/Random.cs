using Unity.Mathematics;
using static Unity.Mathematics.math;

namespace UnityDots.Mathematics
{
    public static class RandomExtensions
    {
        public static float NextLimitedGaussianFloat(ref Random random, float minValue, float maxValue)
        {
            return (float)NextLimitedGaussianDouble(ref random, minValue, maxValue);
        }

        public static float NextGaussianFloat(ref Random random, float mean = 0, float standardDeviation = 1)
        {
            return (float)NextGaussianDouble(ref random, mean, standardDeviation);
        }

        public static int NextLimitedGaussian(ref Random random, int minValue, int maxValue)
        {
            var value = NextLimitedGaussianDouble(ref random, minValue, maxValue);
            return (int)round(value);
        }

        public static double NextLimitedGaussianDouble(ref Random random, double minValue, double maxValue)
        {
            // The deviation is selected so that 99.7% values are between [min, max].
            var mean = (minValue + maxValue) / 2.0;
            var standardDeviation = (maxValue - mean) / 3.0;
            var value = NextGaussianDouble(ref random, mean, standardDeviation);

            // Clamp the remaining 0.3% of values.
            if (value < minValue)
            {
                value = minValue;
            }
            else if (value > maxValue)
            {
                value = maxValue;
            }
            return value;
        }

        public static double NextGaussianDouble(ref Random random, double mean = 0, double standardDeviation = 1)
        {
            var u1 = 1.0 - random.NextDouble();
            var u2 = 1.0 - random.NextDouble();
            var randomStandardNormal = sqrt(-2.0 * log(u1)) * sin(2.0 * PI * u2);
            return mean + (standardDeviation * randomStandardNormal);
        }

        public static float2 NextOnCircle(ref Random random, float radius = 1)
        {
            var direction = random.NextFloat2Direction();
            return direction * radius;
        }

        public static float2 NextInsideCircle(ref Random random, float radius = 1)
        {
            var direction = random.NextFloat2Direction();
            var r = radius * sqrt(random.NextFloat());
            return direction * r;
        }

        public static float3 NextOnSphere(ref Random random, float radius = 1)
        {
            var direction = random.NextFloat3Direction();
            return direction * radius;
        }

        public static float3 NextInsideSphere(ref Random random, float radius = 1)
        {
            var direction = random.NextFloat3Direction();
            var r = radius * pow(random.NextFloat(), 1.0f / 3.0f);
            return direction * r;
        }

        public static float3 NextOnCone(ref Random random, float3 direction, float angle)
        {
            var rotation = Unity.Mathematics.quaternion.LookRotationSafe(direction, new float3(0, 1, 0));
            var z = random.NextFloat(cos(angle), 1);
            var theta = random.NextFloat(0, 2 * PI);
            var scale = sqrt(1 - z * z);
            var vector = new float3(scale * cos(theta), scale * sin(theta), z);
            return rotate(rotation, vector);
        }

        public static int[] Permutation(this ref Random random, int min, int max)
        {
            // For some reason random returns 0 for the first NextInt used.
            random.NextInt();

            var result = new int[max - min + 1];
            for (int i = 0; i < result.Length; ++i)
            {
                result[i] = i;
            }

            for (var i = 0; i < result.Length; ++i)
            {
                var index = random.NextInt(i, result.Length);
                var temp = result[index];
                result[index] = result[i];
                result[i] = temp;
            }

            return result;
        }
    }
}
